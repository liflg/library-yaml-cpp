#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    echo "5): i386-darwin-macos"
    echo "6): x86_64-darwin-macos"
    echo "7): universal-darwin-macos (fat binary i386 and x86_64)"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    elif [ x"$SELECTEDOPTION" = x"5" ]; then
        MULTIARCHNAME=i386-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"6" ]; then
        MULTIARCHNAME=x86_64-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"7" ]; then
        MULTIARCHNAME=universal-darwin-macos
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        BUILDTYPE=Debug
    else
        BUILDTYPE=Release
    fi

    if [ x"$OPTIMIZATION" = xlow ]; then
        echo "Optimization level \"low\" is not yet implemented."
        exit
    fi

    rm -rf "$PREFIXDIR"

    mkdir "$BUILDDIR"
    ( cd "$BUILDDIR"
    cmake ../source \
        -DCMAKE_CXX_FLAGS="$OPTIMIZATION" \
        -DCMAKE_C_FLAGS="$OPTIMIZATION" \
        -DCMAKE_BUILD_TYPE=$BUILDTYPE \
        -DCMAKE_INSTALL_PREFIX="$PREFIXDIR" \
        -DBOOST_ROOT=../../library-boost/i386-linux-gnu/ \
        -DBUILD_SHARED_LIBS=ON
    make
    make install
    rm -rf "$PREFIXDIR"/{lib/*.a,lib/pkgconfig} )
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/LICENSE "$PREFIXDIR"/lib/LICENSE-yaml-cpp.txt

rm -rf "$BUILDDIR"

echo "YAML CPP for $MULTIARCHNAME is ready."
