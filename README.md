Website
=======
https://github.com/jbeder/yaml-cpp/

License
=======
MIT license (see the file source/LICENSE)

Version
=======
0.5.3

Source
======
yaml-cpp-0.5.3.tar.gz (sha256: decc5beabb86e8ed9ebeb04358d5363a5c4f72d458b2c788cb2f3ac9c19467b2)

Requires
========
* boost
